<?php
// Importando PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// llamando variables del formulario de index.php
$nombre = $_POST['nombre'];
$apellido = $_POST['apellido'];
$correo = $_POST['correo'];
$prefijo = $_POST['prefijo'];
$celular = $_POST['celular'];

// Importando archivos del PHPMailer
require '../PHPMailer-master/src/Exception.php';
require '../PHPMailer-master/src/PHPMailer.php';
require '../PHPMailer-master/src/SMTP.php';

// Creando el E-mail
$mail = new PHPMailer();

// Datos del Servidor SMTP
$mail->isSMTP();                      // Set mailer to use SMTP 
$mail->Host = 'mail.abin.world';       // Specify main and backup SMTP servers 
$mail->SMTPAuth = true;               // Enable SMTP authentication 
$mail->Username = 'abinwebinar@abin.world';   // SMTP username 
$mail->Password = 'yJ=7Jmy_-)-2';   // SMTP password 
$mail->SMTPSecure = 'ssl';            // Enable TLS encryption, `ssl` also accepted 
$mail->Port = 465;                    // TCP port to connect to

//Recipients
$mail->setFrom('abinwebinar@abin.world', 'ABIN');
$mail->addAddress($correo);     //Add a recipient
// $mail->addAddress('abinholding@gmail.com');

//Attachments
$mail->AddEmbeddedImage('../Models/Imagenes/webinar.png', 'webinar');    //Optional name

//Content
$mail->isHTML(true);
$mail->CharSet = 'UTF-8';                              //Set email format to HTML
$mail->Subject = ("Te esperamos en este Webinar GRATUITO: “SI APRENDO, EMPRENDO” 🚀");

// Contenido del E-mail
$bodycontent = '<span>¡Hola!</span>';
$bodycontent .= '<br>';
$bodycontent .= '<span>Felicidades por registrarte a este Webinar Gratuito: </span>';
$bodycontent .= '<span style="font-weight: bold;">“¡SI APRENDO, EMPRENDO!” </span>';
$bodycontent .= '<span>donde conocerás la importancia de un </span>';
$bodycontent .= '<span style="font-weight: bold;">Plan de Negocios </span>';
$bodycontent .= '<span>para tu emprendimiento. 🤩</span>';
$bodycontent .= '<br>';
$bodycontent .= '<br>';
$bodycontent .= '<span>🧐 </span>';
$bodycontent .= '<span style="font-style: oblique;">¿Sabes cuál es la principal razón por la que los emprendimientos
fracasan?</span>';
$bodycontent .= '<br>';
$bodycontent .= '<span style="font-weight: bold;">🟥 ¡LA FALTA DE PLANIFICACIÓN!</span>';
$bodycontent .= '<br>';
$bodycontent .= '<br>';
$bodycontent .= '<span>🔉 No dejes pasar esta oportunidad única en la que te enseñaremos a </span>';
$bodycontent .= '<span style="font-weight: bold;">CREAR Y GESTIONAR TU PLAN DE NEGOCIO.🕵️‍♀️</span>';
$bodycontent .= '<br>';
$bodycontent .= '<br>';
$bodycontent .= '<span>🗣️ Te esperamos en el </span>';
$bodycontent .= '<span style="font-weight: bold;">Webinar - ABIN </span>';
$bodycontent .= '<span>Incubadora de Negocios</span>';
$bodycontent .= '<br>';
$bodycontent .= '<span style="font-weight: bold;">🗓 Lunes, 4 de abril</span>';
$bodycontent .= '<br>';
$bodycontent .= '<span style="font-weight: bold;">🕘 6:00 p.m.</span>';
$bodycontent .= '<br>';
$bodycontent .= '<br>';
$bodycontent .= '<span>💻 Únete a la </span>';
$bodycontent .= '<span style="font-weight: bold;">reunión </span>';
$bodycontent .= '<span>a través de este link: </span>';
$bodycontent .= '<a href="https://meet.google.com/esw-grwf-cvm">https://meet.google.com/esw-grwf-cvm</a>';
$bodycontent .= '<br>';
$bodycontent .= '<br>';
$bodycontent .= '<span style="font-weight: bold;">🤩 ¡ESTAMOS ENTUSIASMADOS POR IMPULSAR TU EMPRENDIMIENTO!</span>';

// llamando a la imagen en el E-mail
$bodycontent .= '<div style="text-align: center;">
<img src="cid:webinar" style="height:70%; width:70%;">
</div>';

// Enviando informacion del usuario que se registro
$bodycontent .= '<h5>Estos son mis datos:</h5>';
$bodycontent .= '<table>
<tr>
  <td>Nombre:</td>
  <td>' . $nombre . '</td>
</tr>
<tr>
  <td>Apellido:</td>
  <td>' . $apellido . ' </td>
</tr>
<tr>
  <td>Email:</td>
  <td>' . $correo . '</td>
</tr>
<tr>
  <td>N° de Contacto:</td>
  <td>' . $prefijo . ' ' . $celular . '<td>
</tr>
</table>';

// Juntando toda la informacion de la variable $bodycontent en la funcion Body de PHPMailler
$mail->Body = $bodycontent;

$mail->send();
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="refresh" content="0; ../respuesta.php" />
  <title>Document</title>
</head>

<body>

</body>

</html>