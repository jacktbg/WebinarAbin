<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="./Models/estilos/index.css">
  <title>Document</title>
</head>
<header>
  <div class="row">
    <div class="col-sm-6">
      <img src="./Models/Imagenes/Recurso 18.svg" alt="logo abin" class="logo-img" />
    </div>
    <div class="col-sm-6" onclick="window.open('https://wa.me/51918501389')">
      <img src="./Models/Imagenes/Unete2.png" alt="logo wsp" class="logo-wsp"
        onclick="window.open('https://wa.me/51918501389')" />
    </div>
  </div>
</header>

<body>
  <div class="container">
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
      <div class="col-sm-6">
        <div clas="div-form">
          <form action="./Controllers/enviar.php" method="POST">
            <div class="mb-3">
              <input type="text" class="form-control" id="" name="nombre" class="" placeholder="Nombres" required>
            </div>
            <br>
            <div class="mb-3">
              <input type="text" class="form-control" id="" name="apellido" class="" placeholder="Apellidos" required>
            </div>
            <br>
            <div class="mb-3">
              <input type="email" class="form-control" id="" name="correo" class="" aria-describedby="emailHelp"
                placeholder="Correo Electrónico" required>
            </div>
            <br>
            <div class="row cel">
              <div class="col-sm-5">
                <input type="text" class="form-control" id="" name="prefijo" class="" placeholder="+51" required>
              </div>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="" name="celular" class="" placeholder="Celular" required>
              </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <div class="btn1">
              <button type="submit" class="btn btn-primary">Regístrate</button>
            </div>
          </form>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="mj">
          <img src="./Models/Imagenes/webinar2.png" alt="logo mensaje" class="logo-mj" />
        </div>
        <div class="n">
          <img src="./Models/Imagenes/beneficios.png" alt="logo plan" class="logo-n" />
        </div>
      </div>
    </div>
  </div>
</body>

</html>