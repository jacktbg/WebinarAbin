<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="./Models/estilos/respuesta.css">
  <title>Document</title>
</head>
<header>
  <div class="row">
    <div class="col-sm-6">
      <img src="./Models/Imagenes/Recurso 18.svg" alt="logo abin" class="logo-img" />
    </div>
    <div class="col-sm-6" onclick="window.open('https://wa.me/51918501389')">
      <img src="./Models/Imagenes/Unete2.png" alt="logo wsp" class="logo-wsp"
        onclick="window.open('https://wa.me/51918501389')" />
    </div>
  </div>
</header>

<body>
  <div>
    <div class="center-img">
      <img src="./Models/Imagenes/bienvenido.png" alt="logo welcome" class="logo-w" />
    </div>
  </div>
  <div>
    <div class="center-img">
      <img src="./Models/Imagenes/Bienvenido2.png" alt="logo welcome2" class="logo-w2" />
    </div>
  </div>
  <div>
    <div class="center-img">
      <img src="./Models/Imagenes/4puntos.png" alt="logo puntos" class="logo-p" />
    </div>
  </div>
</body>

</html>